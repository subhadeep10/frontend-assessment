import React from 'react';

import "./MainPanel.css";

class MainPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showAvg:false,
      classAvg:0
    }
  }

  UNSAFE_componentWillMount() {
    
  //  console.log("hoooo",this.props.match );
    if(this.props.match && this.props.match.params.class_id) {
      // console.log(this.props.match.params.class_id);
      this.props.callbackP(this.props.match.params.class_id );
    }
  }
  UNSAFE_componentWillReceiveProps(nextState) {
    // console.log("sa",nextState);
    if(nextState.match.params.class_id !== this.props.match.params.class_id) {
    //  console.log("hoooo",this.props.match );
      if(this.props.match && this.props.match.params.class_id) {
        // console.log(this.props.match.params.class_id);
        this.props.callbackP(nextState.match.params.class_id );
      }
    }
  }
  MarksList = (marks) => {
    return Object.entries(marks).map((e,i) => {
      return (
        <div className="infoMarks" key={i}>
          <span className="infoMarksName">{e[0]}</span>
          <span className="perBar">
            <span className="perBarValue" style={{width:'calc('+e[1]+'%)'}}></span>
          </span>
          <span className="infoMarksPer">{e[1]}%</span>
        </div>
      )
    });
  }
  showAvg = (e) => {
    e.preventDefault();
    const class_id = this.props.match ? this.props.match.params.class_id ?this.props.match.params.class_id:null:null;
    const classDet = this.props.data.filter((e) => { return e.classname === class_id  })[0];
    let classAvg = classDet.students.map((std,i) => {
      let avg = Math.ceil(Object.values(std.marks).reduce((sum,val) => {return sum+=(val)})/3);
      return avg;
    }).reduce((sum,a) => {return sum+=a})/classDet.students.length;

    this.setState({
      showAvg:!this.state.showAvg,
      classAvg: classAvg
    })
  }
  render() {
    // console.log(this.props.match);
    const class_id = this.props.match ? this.props.match.params.class_id ?this.props.match.params.class_id:null:null;
    // console.log(class_id);
    if(class_id==null) {
      return(
        <div className="mainPanel">
          <div className="mainPanelIntro">
            <h1>Select a Class</h1>
          </div>
        </div>
      )
    }
    else {
      const classDet = this.props.data.filter((e) => { return e.classname === class_id  })[0];
      console.log("hell",classDet);
      return (
        <div className="mainPanel">
            <div className="mainPanelCon">
              <div className="mainPanelHeader">
                <div className="mainPanelHeaderCon">
                  <div className="headerBox">
                    <h1>{classDet.classname}</h1>
                    <h4>{classDet.students.length} {classDet.students.length<=1?"student":"students"}</h4>
                  </div>
                  <div className="headerBox">
                    <button onClick={e =>this.showAvg(e)} className={this.state.showAvg?"showAvgBut active":"showAvgBut"}>{this.state.showAvg?"Hide":"Show"} Average</button>
                  </div>
                </div>
                {this.state.showAvg?
                  <div className="headerAddDet">
                    <h3>Classroom Average Percentage: {this.state.classAvg}%</h3>
                  </div>:
                ""}
              </div>
              <div className="mainPanelBody">
                <div className="studentsList">
                  {classDet.students.map((std,i) => {
                    const avg = Math.ceil(Object.values(std.marks).reduce((sum,val) => {return sum+=(val)})/3);
                    return (
                      <div className="studentInfo" key={i}>
                        <div className="infoHeader">
                            <span>{std.name}</span>
                            <span>{avg}%</span>
                        </div>
                        <div className="infoBody">
                          {this.MarksList(std.marks)}
                        </div>
                      </div>
                    )
                  })}

                </div>
              </div>
            </div>
        </div>
      );
    }
  }
}
export default MainPanel;
import React from 'react';
import { Link } from "react-router-dom";

import "./SidePanel.css";

class SidePanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeClass:null
    }
  }
  render() {
    const classesList = this.props.data;
    const class_id = this.props.activeClass;
    // const class_id = (this.props.location.pathname).split('/')[2];
    // console.log(class_id);
    return (
      <div className="sidePanel">
        <div className="panelCon">
          <div className="panelHeader">
              <h2><Link to="/">School:XYZ</Link></h2>
          </div>
          <div className="panelBody">
            <div className="classesList">
                <ul>
                  {classesList.map((cl,i) => {
                    return(
                    <li key={i} className={class_id===cl.classname?"active":""} >
                      <Link to={"/classes/"+cl.classname} >{cl.classname} </Link>
                    </li>
                    )
                  })}
                </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default SidePanel;
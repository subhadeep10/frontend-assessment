import React from 'react';

import MainPanel from '../MainPanel/MainPanel';
import SidePanel from '../SidePanel/SidePanel';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./Layout.css";

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeClass:null
    }
  }
  callbackChild = (data) => {
    // if(this.props.match && this.props.match.params.class_id) {
      this.setState({
        activeClass:  data,
      });
    
    // console.log("dsadsa",this.state.activeClass);
    // console.log("found",this.props);
  }
  render() {
    return (
      <Router>
        <div className="layout">
          {/* <h1> Layout </h1> */}
          <Switch>
            {/* <Route path="/" render={(props) => <SidePanel {...props} data={this.props.data} />} /> */}
            <Route exact path="/" component={MainPanel} callbackP={this.callbackChild} />
            <Route exact path="/classes/:class_id" render={(props) => <MainPanel {...props} data={this.props.data}  callbackP={this.callbackChild}/>} />
          </Switch>
          
          <SidePanel data={this.props.data} activeClass={this.state.activeClass} />
        </div>
      </Router>
    );
  }
}
export default Layout;